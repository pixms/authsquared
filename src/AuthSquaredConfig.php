<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\AuthSquared;

/**
 * Description of AuthenticationConfig
 *
 * @author Sonia
 */
class AuthSquaredConfig
{

    protected $loginPath;
    protected $logoutPath;
    protected $loggedOutMessage;
    protected $loggedInMessage;
    protected $wrongCredentialsMessage;
    protected $unauthorizedMessage;
    protected $loggedInPath;
    protected $usernameParam;
    protected $passwordParam;
    protected $loginGetRoute;
    protected $loginPostRoute;
    protected $logoutGetRoute;
    
    public function __construct($config = []) {
        $this->set($config);
    }
    
    public function set($config)
    {
        if (!is_array($config) && !$config instanceof Traversable && !$config instanceof stdClass) {
            throw new InvalidArgumentException("The configuration must be traversable.");
        }

        foreach ($config as $key => $value) {
            $property = $this->snakeToCamelCase($key);

            if (property_exists($this, $property)) {
                $this->$property = $value;
            } else {
                throw new OutOfBoundsException("The configuration key " . $key . " does not exist.");
            }
        }
    }

    public function getWelcomePath()
    {
        return $this->welcomePath;
    }

    public function getUsernameParam()
    {
        return $this->usernameParam;
    }

    public function getPasswordParam()
    {
        return $this->passwordParam;
    }

    public function getLoginGetRoute()
    {
        return $this->loginGetRoute;
    }

    public function getLoginPostRoute()
    {
        return $this->loginPostRoute;
    }

    public function getLoginPath()
    {
        return $this->loginPath;
    }

    public function getLogoutPath()
    {
        return $this->logoutPath;
    }
    public function getLoggedOutMessage()
    {
        return $this->loggedOutMessage;
    }
    public function getLoggedInMessage()
    {
        return $this->loggedInMessage;
    }
    public function getLoggedInPath()
    {
        return $this->loggedInPath;
    }
    public function getUnauthorizedMessage()
    {
        return $this->unauthorizedMessage;
    }
    public function getWrongCredentialsMessage()
    {
        return $this->wrongCredentialsMessage;
    }

    private function snakeToCamelCase($str)
    {
        $closure = function($matches) {
            return strtoupper($matches[1]);
        };
        return preg_replace_callback('/_([a-z])/', $closure, $str);
    }

}

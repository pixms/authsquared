<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\AuthSquared\Interfaces;

/**
 *
 * @author Sonia
 */
interface UserRepositoryInterface
{
    
    public function getByUsername($username);
    public function getById($id);
    public function getRights(UserInterface $user);
}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\AuthSquared\Interfaces;

/**
 *
 * @author Sonia
 */
interface UserInterface
{
    
    public function getPassword();
    public function getId();
}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\AuthSquared\Interfaces;

use Symfony\Component\HttpFoundation\Request;
/**
 *
 * @author Sonia
 */
interface AuthorizableInterface
{
    public function getRights(Request $request);
}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\AuthSquared\Interfaces;

/**
 * Description of PasswordHandlerInterface
 *
 * @author Sonia
 */
interface PasswordHandlerInterface
{
    public function hash($password);
    
    public function random($length);
    
    public function verify($password, $hash);
}

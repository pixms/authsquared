<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\AuthSquared\Controllers;

use Pixms\AuthSquared\AuthSquared;
use Laasti\Notifications\NotificationService;
use Laasti\Response\ResponderInterface;
use Symfony\Component\HttpFoundation\Request;
/**
 * Description of LogoutController
 *
 * @author Sonia
 */
class LogoutController
{
    protected $service;
    protected $notification;
    protected $responder;
    
    public function __construct(AuthSquared $service, NotificationService $notification, ResponderInterface $responder) {
        $this->service = $service;
        $this->notification = $notification;
        $this->responder = $responder;
    }
    
    public function handle(Request $request) 
    {
        
        if ($this->service->isAuthenticated()) {
            $this->service->logout();
        }
        $this->notification->success($this->service->getConfig()->getLoggedOutMessage());
        return $this->responder->redirect($this->service->getConfig()->getLoginPath());
    }
    
}

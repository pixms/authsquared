<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\AuthSquared\Middlewares;

use Laasti\Notifications\NotificationService;
use Laasti\Stack\MiddlewareInterface;
use Pixms\AuthSquared\AuthSquared;
use Pixms\AuthSquared\Exceptions\UnauthorizedAccessException;
use Pixms\AuthSquared\Interfaces\AuthorizableInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of AuthenticationMiddleware
 *
 * @author Sonia
 */
class AuthorizationMiddleware implements MiddlewareInterface
{
    protected $authentication;
    protected $notification;
    protected $responder;
    protected $controller;

    public function __construct(AuthSquared $authentication, $controller, NotificationService $notification, \Laasti\Response\ResponderInterface $responder)
    {
        $this->authentication = $authentication;
        $this->notification = $notification;
        $this->responder = $responder;
        $this->controller = $controller;
    }
    
    public function handle(Request $request)
    {
        $controller = $request->attributes->get($this->controller);
        if (!$controller instanceof AuthorizableInterface) {
            return $request;
        }
        
        $user = $this->authentication->getAuthenticatedUser();
        $rights = $controller->getRights($request);

        //We need an authenticated user to check the rights, ask to login
        if ($user === false && count($rights) > 0) {
            return $this->responder->redirect($this->authentication->getConfig()->getLoginPath());
        } else if ($user !== false && !$this->authentication->grantAccess($user, $rights)) {
            throw new UnauthorizedAccessException('The user does not have the rights to access this page.');
        }
        
        return $request;
    }
}

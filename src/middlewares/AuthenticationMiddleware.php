<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\AuthSquared\Middlewares;

use Laasti\Route\RouteCollector;
use Laasti\Stack\MiddlewareInterface;
use Pixms\AuthSquared\Exceptions\InvalidCredentialsException;
use Pixms\AuthSquared\Interfaces\UnauthorizedAccessException;
use Pixms\AuthSquared\AuthSquared;
use Laasti\Notifications\NotificationService;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of AuthenticationMiddleware
 *
 * @author Sonia
 */
class AuthenticationMiddleware implements MiddlewareInterface
{
    protected $service;
    protected $responder;
    protected $notification;
    
    public function __construct(AuthSquared $service, NotificationService $notification, \Laasti\Response\ResponderInterface $responder)
    {
        $this->service = $service;
        $this->responder = $responder;
        $this->notification = $notification;
    }

    public function handle(Request $request)
    {
        $config = $this->service->getConfig();

        $pathinfo = $request->getPathInfo();
        
        $isLogoutRoute = preg_match('#^'.$config->getLogoutPath().'$#', $pathinfo);
        $isLoginRoute = preg_match('#^'.$config->getLoginPath().'$#', $pathinfo);
        $isAuthenticated = $this->service->isAuthenticated();
        $username = $request->request->get($config->getUsernameParam());
        $password = $request->request->get($config->getPasswordParam());

        if (!$isAuthenticated && $isLoginRoute && !is_null($username) && !is_null($password)) {
            try {
                $this->service->login($username, $password);
                $isAuthenticated = true;
            } catch (InvalidCredentialsException $e) {
                $this->notification->error($config->getWrongCredentialsMessage());
                return $this->responder->redirect($config->getLoginPath());
            }
        }

        if ($isAuthenticated && $isLoginRoute) {
            $this->notification->success($config->getLoggedInMessage());
            return $this->responder->redirect($config->getLoggedInPath());
        }
        
        try {
            return $request;
        } catch (UnauthorizedAccessException $e) {
            //TODO I want to be able to catch an error here
            // I could add a catch method that the stack will use
        }
    }

    
}

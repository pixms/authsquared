<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\AuthSquared;

use Pixms\AuthSquared\Interfaces\PasswordHandlerInterface;

/**
 * Description of PasswordHandler
 *
 * @author Sonia
 */
class PasswordHandler implements PasswordHandlerInterface
{
    public function hash($password, $algorithm = null, $options = array()) {
        $algorithm = $algorithm ?: PASSWORD_BCRYPT;
        return password_hash($password, $algorithm, $options);
    }
    
    public function random($length = 8) {
        $valid_chars = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = '';
        $chars_length = strlen($valid_chars) - 1;
        for ($i = 0; $i < $length; $i++) {
            $n = rand(0, $chars_length);
            $pass .= $valid_chars[$n];
        }
        return $pass;
    }
    
    public function verify($password, $hash) {
        return password_verify($password, $hash);
    }
    
    public function isFresh($hash, $algorithm = null, $options = array()) {
        $algorithm = $algorithm ?: PASSWORD_BCRYPT;
        return password_needs_rehash($hash, $algorithm, $options);
    }
}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\AuthSquared\Providers;

use League\Container\ServiceProvider;
use Laasti\Providers\RoutableProviderInterface;

/**
 * Description of AuthSquaredServiceProvider
 *
 * @author Sonia
 */
class LeagueProvider extends ServiceProvider implements RoutableProviderInterface
{
    protected $provides = [
        'Pixms\AuthSquared\Interfaces\PasswordHandlerInterface',
        'Pixms\AuthSquared\PasswordHandler',
        'Pixms\AuthSquared\AuthSquared',
        'Pixms\AuthSquared\AuthSquaredConfig',
        'Pixms\AuthSquared\Middlewares\AuthenticationMiddleware',
        'Pixms\AuthSquared\Controllers\LogoutController',
        'Pixms\AuthSquared\Middlewares\AuthorizationMiddleware'
    ];
    
    protected $defaultConfig = [
        'logout_get_route' => 'Pixms\AuthSquared\Controllers\LogoutController::handle'
    ];
    
    protected $config;
    
    public function register()
    {        
        $config = $this->getConfig();
        
        $this->getContainer()->add('Pixms\AuthSquared\Controllers\LogoutController')->withArguments([
            "Pixms\AuthSquared\AuthSquared",
            "Laasti\Notifications\NotificationService",
            "Laasti\Response\ResponderInterface",
        ]);
        if (!$this->getContainer()->isRegistered('Pixms\AuthSquared\PasswordHandler')) {
            $this->getContainer()->add('Pixms\AuthSquared\PasswordHandler');
        }
        if (!$this->getContainer()->isRegistered('Pixms\AuthSquared\AuthSquaredConfig')) {
            $this->getContainer()->add('Pixms\AuthSquared\AuthSquaredConfig')->withArgument($config);
        }
        if (!$this->getContainer()->isRegistered('Pixms\AuthSquared\Interfaces\PasswordHandlerInterface')) {
            $this->getContainer()->add('Pixms\AuthSquared\Interfaces\PasswordHandlerInterface', 'Pixms\AuthSquared\PasswordHandler');
        }
        if (!$this->getContainer()->isRegistered('Pixms\AuthSquared\AuthSquared')) {
            $this->getContainer()->add('Pixms\AuthSquared\AuthSquared', null, true)->withArguments([
                'Pixms\AuthSquared\AuthSquaredConfig', 'Pixms\AuthSquared\Interfaces\UserRepositoryInterface',
                'Symfony\Component\HttpFoundation\Session\SessionInterface', 'Pixms\AuthSquared\Interfaces\PasswordHandlerInterface'
            ]);
        }
        if (!$this->getContainer()->isRegistered('Pixms\AuthSquared\Middlewares\AuthenticationMiddleware')) {
            $this->getContainer()->add('Pixms\AuthSquared\Middlewares\AuthenticationMiddleware')->withArguments([
                'Pixms\AuthSquared\AuthSquared', 'Laasti\Notifications\NotificationService', 'Laasti\Response\ResponderInterface'
            ]);
        }
        if (!$this->getContainer()->isRegistered('Pixms\AuthSquared\Middlewares\AuthorizationMiddleware')) {
            $this->getContainer()->add('Pixms\AuthSquared\Middlewares\AuthorizationMiddleware')->withArguments([
                'Pixms\AuthSquared\AuthSquared', 'Laasti\Route\RouteInterface', 
                'Laasti\Notifications\NotificationService', 'Laasti\Response\ResponderInterface'
            ]);
        }
    }
    
    public function getRoutes() {
        $config = $this->getConfig();
        return [
            ['GET', $config['logout_path'], $config['logout_get_route']],
            ['GET', $config['login_path'], $config['login_get_route']],
            ['POST', $config['login_path'], $config['login_post_route']]
        ];
    }
    
    protected function getConfig() {
        if (is_null($this->config)) {
            $di = $this->getContainer();
            $config = $this->defaultConfig;
            if (isset($di['AuthSquared.config'])) {
                $config = array_merge($this->defaultConfig, $di['AuthSquared.config']);
            } else {
                throw new \RuntimeException('You must provide configuration AuthSquared.config into you DI container.');
            }
            $this->config = $config;
        }
        
        return $this->config;
    }
    
}

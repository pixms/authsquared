<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\AuthSquared;

use Pixms\AuthSquared\Exceptions\InvalidCredentialsException;
use Pixms\AuthSquared\AuthSquaredConfig;
use Pixms\AuthSquared\Interfaces\PasswordHandlerInterface;
use Pixms\AuthSquared\Interfaces\UserRepositoryInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Description of AuthenticationService
 *
 * @author Sonia
 */
class AuthSquared
{
    protected $config;
    protected $repository;
    protected $pwd;
    protected $session;
    protected $user;
    
    public function __construct(AuthSquaredConfig $config, UserRepositoryInterface $repository, SessionInterface $session, PasswordHandlerInterface $pwd)
    {
        $this->config = $config;
        $this->repository = $repository;
        $this->pwd = $pwd;
        $this->session = $session;
    }

    public function login($username, $password) {

        $user = $this->repository->getByUsername($username);
        if ($user && $this->pwd->verify($password, $user->getPassword())) {
            $this->session->set('authSquared.is_authenticated', true);
            $this->session->set('authSquared.user_id', $user->getId());
            return $user;
        } else {
            throw new InvalidCredentialsException;
        }
    }
    
    public function isAuthenticated() {
        return $this->session->get('authSquared.is_authenticated', false);
    }

    public function getAuthenticatedUser() {
        if (is_null($this->user)) {
            $this->user = $this->repository->getById($this->session->get('authSquared.user_id'));
            if ($this->user == false) {
                $this->user = false;
            }
        }
        return $this->user;
    }

    public function logout() {
        $this->session->remove('authSquared.is_authenticated');
        $this->session->remove('authSquared.user_id');
        $this->user = null;
        return true;
    }

    public function getConfig() {
        return $this->config;
    }

    public function grantAccess($user, $rights = []) {
        $user_rights = $this->repository->getRights($user);
        return count(array_intersect($rights, $user_rights)) === count($rights);
    }
}
